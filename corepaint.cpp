/*
	*
	* This file is a part of CorePaint.
	* A paint app for C Suite.
	* Copyright 2019 CuboCore Group
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* (at your option) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/


#include <QMap>
#include <QMenu>
#include <QApplication>
#include <QAction>
#include <QMenuBar>
#include <QMessageBox>
#include <QScrollArea>
#include <QLabel>
#include <QtEvents>
#include <QPainter>
#include <QInputDialog>
#include <QUndoGroup>
#include <QtCore/QTimer>
#include <QtCore/QMap>
#include <QDateTime>
#include <QSettings>
#include <QShortcut>
#include <QScreen>

#include <cprime/messageengine.h>
#include <cprime/themefunc.h>
#include <cprime/pinit.h>
#include <cprime/shareit.h>
#include <cprime/sortfunc.h>
#include <cprime/filefunc.h>
#include <cprime/activitesmanage.h>

#include "imagearea.h"
#include "datasingleton.h"
#include "widgets/colorchooser.h"

#include "corepaint.h"
#include "ui_corepaint.h"


corepaint::corepaint(QWidget *parent): QWidget(parent), ui(new Ui::corepaint)
  , smi(new settings)
  , mPrevInstrumentSetted(false)
{
	ui->setupUi(this);

	mUndoStackGroup = new QUndoGroup(this);

	qRegisterMetaType<InstrumentsEnum>("InstrumentsEnum");
	DataSingleton::Instance()->setIsInitialized();

	loadSettings();
	startSetup();
    setupIcons();
	initializeMainMenu();
	shotcuts();
	loadActivities();
}

corepaint::~corepaint()
{
	delete ui;
}

/**
 * @brief Setup ui elements
 */
void corepaint::startSetup()
{
	ui->appTitle->setAttribute(Qt::WA_TransparentForMouseEvents);
	ui->appTitle->setFocusPolicy(Qt::NoFocus);
	this->resize(800, 500);

    ui->pages->setCurrentIndex(0);

	// Set the transparent color to the main widget
	QPalette p(palette());
	p.setColor(QPalette::Base, Qt::transparent);
	this->setPalette(p);

    if (uiMode == 2) {
        // setup mobile UI
        this->setWindowState(Qt::WindowMaximized);

        ui->activitiesList->setVisible(false);
    } else {
        // setup desktop or tablet UI

        if(windowMaximized){
            this->setWindowState(Qt::WindowMaximized);
            qDebug() << "window is maximized";
        } else{
            this->resize(windowSize);
            qDebug() << windowSize;
        }

        if(activities){
            loadActivities();
        }
    }

	// all toolbuttons icon size in sideView
    QList<QToolButton *> toolBtns = ui->sideView->findChildren<QToolButton *>();
    for (QToolButton *b: toolBtns) {
		if (b) {
            b->setIconSize(toolsIconSize);
		}
	}

	// all toolbuttons icon size in toolBar
    QList<QToolButton *> toolBtns2 = ui->toolBar->findChildren<QToolButton *>();
    for (QToolButton *b: toolBtns2) {
		if (b) {
            b->setIconSize(toolsIconSize);
		}
	}

	if (workFilePath.isEmpty()) {
		ui->save->setEnabled(false);
		ui->saveas->setEnabled(false);
		ui->pinIt->setEnabled(false);
		ui->shareIt->setEnabled(false);
		ui->canvasB->setEnabled(false);
		ui->selectionB->setEnabled(false);
		ui->toolsB->setEnabled(false);
		ui->colorB->setEnabled(false);
		ui->miniTools->hide();
	}

	ui->pages->setCurrentIndex(0);
    ui->page->setCurrentIndex(0);

	ui->paintTabs->setCornerWidget(ui->mSizeLabel, Qt::BottomRightCorner);

    connect(ui->openFiles, SIGNAL(clicked()), this, SLOT(openFileDialog()));
    connect(ui->openFilesB, SIGNAL(clicked()), this, SLOT(openFileDialog()));
    connect(ui->newCanvas, SIGNAL(clicked()), this, SLOT(openNewTab()));
    connect(ui->newCanvasB, SIGNAL(clicked()), this, SLOT(openNewTab()));
}

void corepaint::setupIcons()
{
    ui->openFiles->setIcon(CPrime::ThemeFunc::themeIcon( "document-open-symbolic", "quickopen-file", "document-open-symbolic" ));
    ui->cut->setIcon(CPrime::ThemeFunc::themeIcon( "edit-cut-symbolic", "edit-cut", "edit-cut" ));
    ui->pinIt->setIcon(CPrime::ThemeFunc::themeIcon( "bookmark-new-symbolic", "bookmark-new-symbolic", "bookmark-new" ));
    ui->shareIt->setIcon(CPrime::ThemeFunc::themeIcon( "document-send-symbolic", "document-send-symbolic", "document-send" ));
    ui->copy->setIcon(CPrime::ThemeFunc::themeIcon( "edit-copy-symbolic", "edit-copy", "edit-copy" ));
    ui->past->setIcon(CPrime::ThemeFunc::themeIcon( "edit-paste-symbolic", "edit-paste", "edit-paste" ));
    ui->delet->setIcon(CPrime::ThemeFunc::themeIcon( "edit-delete-symbolic", "edit-delete", "edit-delete" ) );
    ui->newCanvas->setIcon(CPrime::ThemeFunc::themeIcon( "document-new-symbolic", "document-new", "document-new" ));
    ui->undo->setIcon(CPrime::ThemeFunc::themeIcon( "edit-undo-symbolic", "edit-undo", "edit-undo" ));
    ui->redo->setIcon(CPrime::ThemeFunc::themeIcon( "edit-redo-symbolic", "edit-redo", "edit-redo" ));
    ui->select->setIcon(CPrime::ThemeFunc::themeIcon( "edit-select-all-symbolic", "edit-select-all", "edit-select-all" ));
    ui->save->setIcon(CPrime::ThemeFunc::themeIcon( "document-save-symbolic", "document-save", "document-save" ));
    ui->saveas->setIcon(CPrime::ThemeFunc::themeIcon( "document-save-as-symbolic", "document-save-as", "document-save-as" ));
    ui->menu->setIcon(CPrime::ThemeFunc::themeIcon( "open-menu-symbolic", "application-menu", "open-menu" ));
    ui->rotateright->setIcon(CPrime::ThemeFunc::themeIcon( "object-rotate-right-symbolic", "object-rotate-right", "object-rotate-right" ));
    ui->rotateleft->setIcon(CPrime::ThemeFunc::themeIcon( "object-rotate-left-symbolic", "object-rotate-left", "object-rotate-left" ));
    ui->resizecanvas->setIcon(CPrime::ThemeFunc::themeIcon( "zoom-fit-best-symbolic", "zoom-fit-best", "zoom-fit-best" ));
    ui->resizeimage->setIcon(CPrime::ThemeFunc::themeIcon( "zoom-original-symbolic", "zoom-original", "zoom-original" ));
    ui->canvasB->setIcon(CPrime::ThemeFunc::themeIcon( "zoom-fit-best-symbolic", "zoom-fit-best", "zoom-fit-best" ));
    ui->colorB->setIcon(CPrime::ThemeFunc::themeIcon( "color-select-symbolic", "color-select", "color-select" ));
    ui->selectionB->setIcon(CPrime::ThemeFunc::themeIcon( "edit-select-all-symbolic", "edit-select-all", "edit-select-all" ));
    ui->toolsB->setIcon(CPrime::ThemeFunc::themeIcon( "document-edit-symbolic", "edit-rename", "document-edit" ));
}

/**
 * @brief Loads application settings
 */
void corepaint::loadSettings()
{
    // get CSuite's settings
    toolsIconSize = smi->getValue("CoreApps", "ToolsIconSize");
    uiMode = smi->getValue("CoreApps", "UIMode");
    activities = smi->getValue("CoreApps", "KeepActivities");
    listViewIconSize = smi->getValue("CoreApps", "ListViewIconSize");

    // get app's settings
    windowSize = smi->getValue("CorePaint", "WindowSize");
    windowMaximized = smi->getValue("CorePaint", "WindowMaximized");
}

void corepaint::shotcuts()
{
	QShortcut *shortcut;
	shortcut = new QShortcut(QKeySequence(Qt::ControlModifier | Qt::Key_O), this);
    connect(shortcut, &QShortcut::activated, this, &corepaint::openFileDialog);
	shortcut = new QShortcut(QKeySequence(Qt::ControlModifier | Qt::Key_S), this);
	connect(shortcut, &QShortcut::activated, this, &corepaint::on_save_clicked);
	shortcut = new QShortcut(QKeySequence(Qt::ControlModifier | Qt::ShiftModifier | Qt::Key_S), this);
	connect(shortcut, &QShortcut::activated, this, &corepaint::on_saveas_clicked);
	shortcut = new QShortcut(QKeySequence(Qt::ControlModifier | Qt::Key_Z), this);
	connect(shortcut, SIGNAL(activated()), this, SLOT(undo()));
	shortcut = new QShortcut(QKeySequence(Qt::ControlModifier | Qt::Key_Y), this);
	connect(shortcut, SIGNAL(activated()), this, SLOT(redo()));
	shortcut = new QShortcut(QKeySequence(Qt::ControlModifier | Qt::Key_C), this);
	connect(shortcut, &QShortcut::activated, this, &corepaint::on_copy_clicked);
	shortcut = new QShortcut(QKeySequence(Qt::ControlModifier | Qt::Key_X), this);
	connect(shortcut, &QShortcut::activated, this, &corepaint::on_cut_clicked);
	shortcut = new QShortcut(QKeySequence(Qt::ControlModifier | Qt::Key_V), this);
	connect(shortcut, &QShortcut::activated, this, &corepaint::on_past_clicked);
	shortcut = new QShortcut(QKeySequence(Qt::ControlModifier | Qt::Key_N), this);
    connect(shortcut, &QShortcut::activated, this, &corepaint::openNewTab);
	shortcut = new QShortcut(QKeySequence(Qt::ControlModifier | Qt::Key_B), this);
	connect(shortcut, &QShortcut::activated, this, &corepaint::on_pinIt_clicked);
	shortcut = new QShortcut(QKeySequence(Qt::Key_Delete), this);
	connect(shortcut, &QShortcut::activated, this, &corepaint::on_delet_clicked);
}

void corepaint::loadActivities()
{
    if(not activities){
        ui->activitiesList->setVisible(false);
        return;
    }

    ui->activitiesList->clear();

    QSettings recentActivity(CPrime::Variables::CC_ActivitiesFilePath(), QSettings::IniFormat);
    QStringList topLevel = recentActivity.childGroups();

    if (topLevel.length()) {
        topLevel = CPrime::SortFunc::sortDate(topLevel, Qt::DescendingOrder);
    }

    QListWidgetItem *item;

    Q_FOREACH (QString group, topLevel) {
        recentActivity.beginGroup(group);
        QStringList keys = recentActivity.childKeys();
        keys = CPrime::SortFunc::sortTime(keys, Qt::DescendingOrder, "hh.mm.ss.zzz");

        Q_FOREACH (QString key, keys) {
            QStringList value = recentActivity.value(key).toString().split("\t\t\t");
            if (value[0] == "corepaint") {
                QString filePath = value[1];
                if (not CPrime::FileUtils::exists(filePath))
                    continue;

                QString baseName = CPrime::FileUtils::baseName(filePath);
                item = new QListWidgetItem(baseName);
                item->setData(Qt::UserRole, filePath);
                item->setToolTip(filePath);
                item->setIcon(CPrime::ThemeFunc::getFileIcon(filePath));
                ui->activitiesList->addItem(item);
            }
        }

        recentActivity.endGroup();
    }
}

void corepaint::initializeNewTab(const bool &isOpen, const QString &filePath)
{
	ui->page->setCurrentIndex(1);

	if (ui->paintTabs->count() < 4) {
		ImageArea *imageArea;
		QString fileName;
		bool newCanvas = false;

		if (isOpen && filePath.isEmpty()) {
			imageArea = new ImageArea(isOpen, "", this);
			fileName = imageArea->getFileName();

			if (!fileName.length()) {
				ui->page->setCurrentIndex(0);
				delete imageArea;
				return;
			}
		} else if (isOpen && !filePath.isEmpty()) {
			imageArea = new ImageArea(isOpen, filePath, this);
			fileName = imageArea->getFileName();
		} else {
			imageArea = new ImageArea(false, "", this);
			fileName = (tr("UntitledImage_") + QString::number(ui->paintTabs->count()));
			newCanvas = true;
		}

		if (!imageArea->getFileName().isNull()) {
			QScrollArea *scrollArea = new QScrollArea();
			scrollArea->setAttribute(Qt::WA_DeleteOnClose);

			//scrollArea->setBackgroundRole(QPalette::Shadow);
			scrollArea->setFrameStyle(QFrame::NoFrame);

			scrollArea->setWidget(imageArea);

			ui->paintTabs->addTab(scrollArea, fileName);
			ui->paintTabs->setCurrentIndex(ui->paintTabs->count() - 1);

			mUndoStackGroup->addStack(imageArea->getUndoStack());
			connect(imageArea, SIGNAL(sendPrimaryColorView()), this, SLOT(setPrimaryColorView()));
			connect(imageArea, SIGNAL(sendSecondaryColorView()), this, SLOT(setSecondaryColorView()));
			connect(imageArea, SIGNAL(sendRestorePreviousInstrument()), this,
					SLOT(restorePreviousInstrument()));
			connect(imageArea, SIGNAL(sendSetInstrument(InstrumentsEnum)), this,
					SLOT(setInstrument(InstrumentsEnum)));
			connect(imageArea, SIGNAL(sendNewImageSize(QSize)), this,
					SLOT(setNewSizeToSizeLabel(QSize)));
			connect(imageArea, SIGNAL(sendEnableCopyCutActions(bool)), this,
					SLOT(enableCopyCutActions(bool)));
			connect(imageArea, SIGNAL(sendEnableSelectionInstrument(bool)), this,
					SLOT(instumentsAct(bool)));
		} else {
			delete imageArea;
		}

		workFilePath = fileName;

		if (ui->paintTabs->count() >= 1) {
			ui->save->setEnabled(true);
			ui->saveas->setEnabled(true);
			ui->pinIt->setEnabled(true);
			ui->shareIt->setEnabled(true);
			ui->canvasB->setEnabled(true);
			ui->selectionB->setEnabled(true);
			ui->toolsB->setEnabled(true);
			ui->colorB->setEnabled(true);
			ui->miniTools->show();
		}

		if ((filePath.length() || fileName.length()) && !newCanvas) {
            CPrime::MessageEngine::showMessage(
				"org.cubocore.CorePaint",
				"CorePaint",
				"File opened",
                "File opened successfully"
			);
		} else {
            // Function from LibCPrime
            CPrime::MessageEngine::showMessage(
				"org.cubocore.CorePaint",
				"CorePaint",
				"Opened new canvas",
				"Draw your precious mind"
			);
		}
	} else {
        // Function from LibCPrime
        CPrime::MessageEngine::showMessage(
			"org.cubocore.CorePaint",
			"CorePaint",
			"Can not open new file or canvas",
			"You have reached the maximum limit of opened tabs"
		);
	}
}

int corepaint::tabsCount()
{
	return ui->paintTabs->count();
}

void corepaint::openNewTab()
{
    initializeNewTab();
}

void corepaint::initializeMainMenu()
{
	QAction *tempAct = mUndoStackGroup->createUndoAction(this);
	tempAct->setIcon(QIcon::fromTheme("edit-undo"));
	ui->undo->setDefaultAction(tempAct);

	tempAct = mUndoStackGroup->createRedoAction(this);
	tempAct->setIcon(QIcon::fromTheme("edit-redo"));
	ui->redo->setDefaultAction(tempAct);

	mInstrumentsMap.clear();
	mInstrumentsMap.insert(CURSOR, ui->select);
	mInstrumentsMap.insert(ERASER, ui->eraser);
	mInstrumentsMap.insert(COLORPICKER, ui->colorpicker);
	mInstrumentsMap.insert(PEN, ui->pen);
	mInstrumentsMap.insert(LINE, ui->line);
	mInstrumentsMap.insert(SPRAY, ui->spray);
	mInstrumentsMap.insert(FILL, ui->fill);
	mInstrumentsMap.insert(RECTANGLE, ui->rectangle);
	mInstrumentsMap.insert(ELLIPSE, ui->ellipse);
	mInstrumentsMap.insert(CURVELINE, ui->curve);
	mInstrumentsMap.insert(TEXT, ui->text);

	Q_FOREACH(QToolButton *btn, mInstrumentsMap) {
		connect(btn, &QToolButton::toggled, this, &corepaint::instumentsAct);
	}

	mPColorChooser = new ColorChooser(0, 0, 0, this);
	mPColorChooser->setStatusTip(tr("Primary color"));
	mPColorChooser->setToolTip(tr("Primary color"));
	connect(mPColorChooser, SIGNAL(sendColor(QColor)), this,
			SLOT(primaryColorChanged(QColor)));

	mSColorChooser = new ColorChooser(255, 255, 255, this);
	mSColorChooser->setStatusTip(tr("Secondary color"));
	mSColorChooser->setToolTip(tr("Secondary color"));
	connect(mSColorChooser, SIGNAL(sendColor(QColor)), this, SLOT(secondaryColorChanged(QColor)));

	ui->cc->addWidget(mPColorChooser);
	ui->cc->addWidget(mSColorChooser);

	connect(ui->toolSize, SIGNAL(valueChanged(int)), this, SLOT(penValueChanged(int)));
}

void corepaint::penValueChanged(const int &value)
{
	DataSingleton::Instance()->setPenSize(value);
}

void corepaint::primaryColorChanged(const QColor &color)
{
	DataSingleton::Instance()->setPrimaryColor(color);
}

void corepaint::secondaryColorChanged(const QColor &color)
{
	DataSingleton::Instance()->setSecondaryColor(color);
}

void corepaint::setPrimaryColorView()
{
	mPColorChooser->setColor(DataSingleton::Instance()->getPrimaryColor());
}

void corepaint::setSecondaryColorView()
{
	mSColorChooser->setColor(DataSingleton::Instance()->getSecondaryColor());
}

ImageArea *corepaint::getCurrentImageArea()
{
	if (ui->paintTabs->currentWidget()) {
		QScrollArea *tempScrollArea = qobject_cast<QScrollArea *>(ui->paintTabs->currentWidget());
		ImageArea *tempArea = qobject_cast<ImageArea *>(tempScrollArea->widget());
		return tempArea;
	}

	return nullptr;
}

ImageArea *corepaint::getImageAreaByIndex(int index)
{
	QScrollArea *sa = static_cast<QScrollArea *>(ui->paintTabs->widget(index));
	ImageArea *ia = static_cast<ImageArea *>(sa->widget());
	return ia;
}

void corepaint::setNewSizeToSizeLabel(const QSize &size)
{
	ui->mSizeLabel->setText(QString("%1 x %2").arg(size.width()).arg(size.height()));
}

bool corepaint::closeAllTabs()
{
	while (ui->paintTabs->count() != 0) {
		ImageArea *ia = getImageAreaByIndex(0);

		if (ia->getEdited()) {
			QString msg = QString("This file contains unsaved changes.\nWould you like to save now?");
			QMessageBox message(QMessageBox::Question, QString("Save Changes"), msg,
								QMessageBox::Yes | QMessageBox::Default | QMessageBox::No | QMessageBox::Cancel |
								QMessageBox::Escape, this);
			message.setWindowIcon(QIcon(":/icons/org.cubocore.CorePaint.svg"));

			int reply = message.exec();

			switch (reply) {
				case QMessageBox::Yes:
					if (ia->save()) {
						break;
					}

					return false;

				case QMessageBox::Cancel:
					return false;
			}
		}

        if (QFileInfo::exists(ia->mFilePath) && activities) {
            qDebug() << "Corepaint: saved to recent " << CPrime::ActivitiesManage::saveToActivites("corepaint", QStringList()<< ia->mFilePath);
		}

		QWidget *wid = ui->paintTabs->widget(0);
		ui->paintTabs->removeTab(0);
		delete wid;
	}

	return true;
}

void corepaint::setAllInstrumentsUnchecked(/*QAction *action*/QToolButton *actBtn)
{
	clearImageSelection();

//    foreach (QAction *temp, mInstrumentsActMap) {
//		if (temp != action) {
//			temp->setChecked(false);
//		}
//	}

	Q_FOREACH(QToolButton *btn, mInstrumentsMap) {
		if (btn != actBtn) {
			btn->setChecked(false);
		}
	}
}

void corepaint::setInstrumentChecked(InstrumentsEnum instrument)
{
	setAllInstrumentsUnchecked(nullptr);

	if (instrument == NONE_INSTRUMENT || instrument == INSTRUMENTS_COUNT) {
		return;
    }

	mInstrumentsMap[instrument]->setChecked(true);
}

void corepaint::instumentsAct(bool state)
{
	QToolButton *currbtn = static_cast<QToolButton *>(sender());

    if (state) {
		if (currbtn == mInstrumentsMap[COLORPICKER] && !mPrevInstrumentSetted) {
			DataSingleton::Instance()->setPreviousInstrument(DataSingleton::Instance()->getInstrument());
			mPrevInstrumentSetted = true;
		}

		setAllInstrumentsUnchecked(currbtn);
		currbtn->setChecked(true);
		DataSingleton::Instance()->setInstrument(mInstrumentsMap.key(currbtn));
		emit sendInstrumentChecked(mInstrumentsMap.key(currbtn));
	} else {
		setAllInstrumentsUnchecked(nullptr);
		DataSingleton::Instance()->setInstrument(NONE_INSTRUMENT);
		emit sendInstrumentChecked(NONE_INSTRUMENT);

		if (currbtn == mInstrumentsMap[CURSOR]) {
			DataSingleton::Instance()->setPreviousInstrument(mInstrumentsMap.key(currbtn));
		}
	}
}

void corepaint::enableCopyCutActions(bool enable)
{
	ui->copy->setEnabled(enable);
	ui->cut->setEnabled(enable);
}

void corepaint::clearImageSelection()
{
	if (getCurrentImageArea()) {
		getCurrentImageArea()->clearSelection();
		DataSingleton::Instance()->setPreviousInstrument(NONE_INSTRUMENT);
	}
}

void corepaint::restorePreviousInstrument()
{
	setInstrumentChecked(DataSingleton::Instance()->getPreviousInstrument());
	DataSingleton::Instance()->setInstrument(DataSingleton::Instance()->getPreviousInstrument());
	emit sendInstrumentChecked(DataSingleton::Instance()->getPreviousInstrument());
	mPrevInstrumentSetted = false;
}

void corepaint::setInstrument(InstrumentsEnum instrument)
{
	setInstrumentChecked(instrument);
	DataSingleton::Instance()->setInstrument(instrument);
	emit sendInstrumentChecked(instrument);
	mPrevInstrumentSetted = false;
}

void corepaint::on_paintTabs_currentChanged(int index)
{
	//activateTab
	if (index == -1) {
		return;
	}

	ui->paintTabs->setCurrentIndex(index);
	getCurrentImageArea()->clearSelection();

	QSize size = getCurrentImageArea()->getImage()->size();
	ui->mSizeLabel->setText(QString("%1 x %2").arg(size.width()).arg(size.height()));

	if (!getCurrentImageArea()->getFileName().isEmpty()) {
		workFilePath = QDir(workFilePath).path() + "/" + getCurrentImageArea()->getFileName();
	} else {
		workFilePath = "Untitled Image";
	}

	mUndoStackGroup->setActiveStack(getCurrentImageArea()->getUndoStack());

	//enableActions
	//if index == -1 it means, that there is no tabs
	bool isEnable = index == -1 ? false : true;

	if (!isEnable) {
		setAllInstrumentsUnchecked(nullptr);
		DataSingleton::Instance()->setInstrument(NONE_INSTRUMENT);
		emit sendInstrumentChecked(NONE_INSTRUMENT);
	}

	this->setWindowTitle(ui->paintTabs->tabText(index) + " - CorePaint");
}

void corepaint::on_paintTabs_tabCloseRequested(int index)
{
	ImageArea *ia = getImageAreaByIndex(index);

	if (ia->getEdited()) {
		QString msg = QString("This file contains unsaved changes.\nWould you like to save now?");
		QMessageBox message(QMessageBox::Question, QString("Save Changes"), msg,
							QMessageBox::Yes | QMessageBox::Default |
							QMessageBox::No | QMessageBox::Cancel | QMessageBox::Escape, this);
		message.setWindowIcon(QIcon(":/icons/org.cubocore.CorePaint.svg"));

		int reply = message.exec();

		switch (reply) {
			case QMessageBox::Yes:
				if (ia->save()) {
					break;
				}

				return;

			case QMessageBox::Cancel:
				return;
		}
	}

    if (QFileInfo::exists(ia->mFilePath) && activities) {
        qDebug() << "Corepaint: saved to recent " << CPrime::ActivitiesManage::saveToActivites("corepaint", QStringList()<< ia->mFilePath);
    }

	mUndoStackGroup->removeStack(ia->getUndoStack());   //for safety
	QWidget *wid = ui->paintTabs->widget(index);
	ui->paintTabs->removeTab(index);
	delete wid;

	if (ui->paintTabs->count() == 0) {
		ui->save->setEnabled(false);
		ui->saveas->setEnabled(false);
		ui->pinIt->setEnabled(false);
		ui->shareIt->setEnabled(false);
		ui->canvasB->setEnabled(false);
		ui->selectionB->setEnabled(false);
		ui->toolsB->setEnabled(false);
		ui->colorB->setEnabled(false);
		ui->miniTools->hide();
        on_menu_clicked();
	}
}

void corepaint::openFileDialog()
{
	initializeNewTab(true);
}

void corepaint::on_save_clicked()
{
	if (getCurrentImageArea()->save()) {
		ui->paintTabs->setTabText(ui->paintTabs->currentIndex(),
								  getCurrentImageArea()->getFileName().isEmpty() ?
								  tr("Untitled Image") : getCurrentImageArea()->getFileName());

		workFilePath = getCurrentImageArea()->mFilePath;
        // Function from LibCPrime
        CPrime::MessageEngine::showMessage(
			"org.cubocore.CorePaint",
			"CorePaint",
			"File Save",
            "File saved successfully"
		);
	} else {
        // Function from LibCPrime
        CPrime::MessageEngine::showMessage(
			"org.cubocore.CorePaint",
			"CorePaint",
			"File not saved",
            "CorePaint was unable to save the file."
		);
	}
}

void corepaint::on_saveas_clicked()
{
	if (getCurrentImageArea()->saveAs()) {
		ui->paintTabs->setTabText(ui->paintTabs->currentIndex(),
								  getCurrentImageArea()->getFileName().isEmpty() ?
								  tr("Untitled Image") : getCurrentImageArea()->getFileName());

		workFilePath = getCurrentImageArea()->mFilePath;
        // Function from LibCPrime
        CPrime::MessageEngine::showMessage(
			"org.cubocore.CorePaint",
			"CorePaint",
			"File Save",
            "File saved successfully"
		);
	} else {
        // Function from LibCPrime
        CPrime::MessageEngine::showMessage(
			"org.cubocore.CorePaint",
			"CorePaint",
			"File not saved",
            "CorePaint was unable to save the file."
		);
	}
}

void corepaint::on_resizeimage_clicked()
{
	getCurrentImageArea()->resizeImage();
}

void corepaint::on_resizecanvas_clicked()
{
	getCurrentImageArea()->resizeCanvas();
}

void corepaint::on_rotateright_clicked()
{
	getCurrentImageArea()->rotateImage(true);
}

void corepaint::on_rotateleft_clicked()
{
	getCurrentImageArea()->rotateImage(false);
}

//void corepaint::on_zoomin_clicked()
//{
//    getCurrentImageArea()->zoomImage(2.0);
//    getCurrentImageArea()->setZoomFactor(2.0);
//}

//void corepaint::on_zoomout_clicked()
//{
//    getCurrentImageArea()->zoomImage(0.5);
//    getCurrentImageArea()->setZoomFactor(0.5);
//}

void corepaint::on_cut_clicked()
{
	if (ImageArea *imageArea = getCurrentImageArea()) {
		imageArea->cutImage();
	}
}

void corepaint::on_copy_clicked()
{
	if (ImageArea *imageArea = getCurrentImageArea()) {
		imageArea->copyImage();
	}
}

void corepaint::on_past_clicked()
{
	if (ImageArea *imageArea = getCurrentImageArea()) {
		imageArea->pasteImage();
	}
}

void corepaint::undo()
{
	mUndoStackGroup->undo();
}

void corepaint::redo()
{
	mUndoStackGroup->redo();
}

void corepaint::on_delet_clicked()
{
	if (ImageArea *imageArea = getCurrentImageArea()) {
		imageArea->clearBackground();
	}
}

void corepaint::pageClick(QToolButton *btn, int i)
{
    // all button checked false
    QList<QToolButton *> toolBtns2 = ui->toolBar->findChildren<QToolButton *>();
    for (QToolButton *b: toolBtns2) {
		b->setChecked(false);
	}
    if (uiMode == 2){
        if(ui->sideView->isVisible()){
            if(ui->pages->currentIndex() == i)
                ui->sideView->setVisible(0);
        } else{

            ui->sideView->setVisible(1);
        }
    }

	btn->setChecked(true);
	ui->pages->setCurrentIndex(i);
}

void corepaint::on_menu_clicked()
{
    pageClick(ui->menu, 0);
}

void corepaint::on_canvasB_clicked()
{
	pageClick(ui->canvasB, 1);
}

void corepaint::on_selectionB_clicked()
{
	pageClick(ui->selectionB, 2);
}

void corepaint::on_toolsB_clicked()
{
	pageClick(ui->toolsB, 3);
}

void corepaint::on_colorB_clicked()
{
	pageClick(ui->colorB, 4);
}

void corepaint::sendFiles(const QStringList &paths)
{
	if (paths.length()) {
		foreach (QString str, paths) {
			if (CPrime::FileUtils::isFile( str ) ) {
				initializeNewTab( true, str );
			} else {
				qDebug() << "func(sendFiles) : Error : Invalid filepath got :" << str;
			}
		}
	}
}

void corepaint::on_pinIt_clicked()
{
	if (!QFile(workFilePath).exists()) {
		// Function from LibCPrime
		QString mess = "Can't pin \nFile: " + workFilePath + "'\ndoes not exist.";
        // Function from LibCPrime
        CPrime::MessageEngine::showMessage(
			"org.cubocore.CorePaint",
			"CorePaint",
			"Error!!!",
            mess
		);
    } else {
		PinIT pit(QStringList() << workFilePath, this);
		pit.exec();
	}
}


void corepaint::on_shareIt_clicked()
{
	if (!QFile(workFilePath).exists()) {
		// Function from LibCPrime
		QString mess = "Can't share \nFile: " + workFilePath + "'\ndoes not exist.";
        // Function from LibCPrime
        CPrime::MessageEngine::showMessage(
			"org.cubocore.CorePaint",
			"CorePaint",
			"Error!!!",
            mess
		);
	} else {
        ShareIT *t = new ShareIT(QStringList() << workFilePath, listViewIconSize, nullptr);
        // Set ShareIT window size
        if (uiMode == 2 )
            t->setFixedSize(QGuiApplication::primaryScreen()->size() * .8 );
        else
            t->resize(500,600);

        t->exec();
	}
}

void corepaint::on_activitiesList_itemDoubleClicked(QListWidgetItem *item)
{
	QString filepath = item->data(Qt::UserRole).toString();
	initializeNewTab(true, filepath);
}

void corepaint::closeEvent(QCloseEvent *event)
{
    event->ignore();

    qDebug()<< "save window stats"<< this->size() << this->isMaximized();
    smi->setValue("CorePaint", "WindowSize", this->size());
    smi->setValue("CorePaint", "WindowMaximized", this->isMaximized());

    if (closeAllTabs()) {
        event->accept();
    }
}
